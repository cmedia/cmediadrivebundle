<?php

/*
 * This file is part of the CMedia Bundle
 *
 * (c) 2013 Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\Entity;

use Doctrine\ORM\Mapping as ORM,
    Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Folder {

    /**
     * @ORM\Id @ORM\GeneratedValue @ORM\Column(type="integer")
     * @var integer
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="\CMedia\Bundle\DriveBundle\Entity\Document", mappedBy="folder", cascade={"persist", "remove"})
     * @var Document
     */
    protected $documents;

    /** 
     * @ORM\Column(type="string", length=255, unique=true) 
     * @Assert\NotBlank()
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string 
     */
    protected $path;

    /**
     * @ORM\ManyToOne(targetEntity="\CMedia\Bundle\DriveBundle\Model\FolderOwnerInterface", inversedBy="folders")
     * @var FolderOwnerInterface
     */
    protected $owner;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath() 
    {
        return $this->getUploadDir() . '/' . $this->path;
    }

    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../../../../../web/' . $this->getUploadDir();
    }
    
    public function getUploadDir()
    {
        return 'uploads/drive';
    }

    /** @ORM\PostPersist() */
    public function makeDir()
    {
        try {
            mkdir($this->getAbsolutePath(), 01777);  
        } catch(Exception $e) {
            echo "The name of the folder should be unique. " . $e;
        }
    }

    /** @ORM\PreRemove() */
    public function setDirForRemove()
    {
        $this->dirForRemove = $this->getAbsolutePath();
    }

    /** @ORM\PostRemove() */
    public function removeDir()
    {
        if ($this->dirForRemove) {
            rmdir($this->dirForRemove);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Folder
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /** @ORM\PrePersist() */
    public function setPath()
    {
        $this->path = sha1(uniqid(mt_rand() . 'cmdrive', true));
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Folder
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add documents
     *
     * @param \CMedia\Bundle\DriveBundle\Entity\Document $documents
     * @return Folder
     */
    public function addDocument(\CMedia\Bundle\DriveBundle\Entity\Document $documents)
    {
        $this->documents[] = $documents;
    
        return $this;
    }

    /**
     * Remove documents
     *
     * @param \CMedia\Bundle\DriveBundle\Entity\Document $documents
     */
    public function removeDocument(\CMedia\Bundle\DriveBundle\Entity\Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set owner
     *
     * @param \CMedia\Bundle\DriveBundle\Model\FolderOwnerInterface $owner
     * @return Folder
     */
    public function setOwner(\CMedia\Bundle\DriveBundle\Model\FolderOwnerInterface $owner = null)
    {
        $this->owner = $owner;
    
        return $this;
    }

    /**
     * Get owner
     *
     * @return \CMedia\Bundle\DriveBundle\Model\FolderOwnerInterface 
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
