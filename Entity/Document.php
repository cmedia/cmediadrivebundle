<?php

/*
 * This file is part of the CMedia Bundle
 *
 * (c) 2013 Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Document
{
    /**
     * @ORM\Id @ORM\GeneratedValue @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\CMedia\Bundle\DriveBundle\Entity\Folder", inversedBy="documents")
     */
    protected $folder;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    protected $path;
    
    /** 
     * @Assert\File(maxSize="614400") 
     * @Assert\NotBlank() 
     **/
    protected $file;

    /** @ORM\PrePersist() */
    public function preUpload()
    {
        if (null !== $this->file) {
            $filename = sha1(uniqid(mt_rand() . 'cmdocument', true));
            $this->path = $filename . '.' . $this->file->guessExtension();
        }
    }

    /** @ORM\PostPersist() */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->file->move(
            $this->getUploadRootDir(),
            $this->path
        );

        unset($this->file);
    }

    /** @ORM\PostRemove() */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return $this->getFolder()->getAbsolutePath() . '/' . $this->path;
    }

    public function getWebPath()
    {
        return $this->getFolder()->getWebPath() . '/' . $this->path;
    }

    public function getUploadRootDir()
    {
        return $this->getFolder()->getAbsolutePath();
    }

    public function setFile(File $file)
    {
        $this->file = $file;
        
        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Document
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set folder
     *
     * @param \CMedia\Bundle\DriveBundle\Entity\Folder $folder
     * @return Document
     */
    public function setFolder(\CMedia\Bundle\DriveBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    
        return $this;
    }

    /**
     * Get folder
     *
     * @return \CMedia\Bundle\DriveBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }
}
