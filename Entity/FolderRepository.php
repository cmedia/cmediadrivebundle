<?php

namespace CMedia\Bundle\DriveBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FolderRepository extends EntityRepository
{
	public function create()
	{

	}

	public function read()
	{
		
	}

	public function update()
	{

	}

	public function delete()
	{

	}

	public function listQuery()
	{

	}

	public function getById()
	{

	}

	public function getByUser()
	{

	}
}