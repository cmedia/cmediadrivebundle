<?php

namespace CMedia\Bundle\DriveBundle\Service;


class DriveManager
{
	public function __construct()
	{
	    $rootDir = $this->get('kernel')->getRootDir() . '/../web';
	}

	public function getFolder()
	{

	}

	public function getDocument()
	{

	}

	public function getAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath() 
    {
        return $this->getUploadDir() . '/' . $this->path;
    }

    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir()
    {
        return 'uploads/collections';
    }

    /** @ORM\PostPersist() */
    public function makeDir()
    {
        try {
            mkdir($this->getAbsolutePath(), 01777);  
        } catch(Exception $e) {
            echo "The name of the folder should be unique. " . $e;
        }
    }

    /** @ORM\PreRemove() */
    public function setDirForRemove()
    {
        $this->dirForRemove = $this->getAbsolutePath();
    }

    /** @ORM\PostRemove() */
    public function removeDir()
    {
        if ($this->dirForRemove) {
            rmdir($this->dirForRemove);
        }
    }

	/*
	public function listFolders()
	{

	}

	public function listFolder()
	{

	}

	public function createFolder()
	{

	}

	public function updateFolder()
	{

	}

	public function deleteFolder()
	{

	}

	public function getDocument()
	{

	}

	public function createDocument()
	{

	}

	public function updateDocument()
	{

	}

	public function deleteDocument()
	{

	} 
	*/
}