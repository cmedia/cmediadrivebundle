<?php

namespace CMedia\Bundle\DriveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CMedia\Bundle\DriveBundle\Entity\Folder;

// use FOS\RestBundle\View\View;

/**
 * Folder controller.
 *
 */
class FolderController extends Controller
{
    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {
        
    }
}
