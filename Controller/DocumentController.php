<?php

namespace CMedia\Bundle\DriveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CMedia\Bundle\DriveBundle\Entity\Document;

// use FOS\RestBundle\View\View;

/**
 * Folder controller.
 *
 */
class DocumentController extends Controller
{
    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {
        
    }
}