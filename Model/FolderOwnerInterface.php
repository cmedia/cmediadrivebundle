<?php

/*
 * This file is part of the CMedia Bundle
 *
 * (c) 2013 Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\Model;

/**
 * FolderOwnerInterface 
 * 
 * An interface that the folders Owner (user) should implement. 
 * Only a single object should implement this interface as the 
 * ResolveTargetEntityListener can only change the target to single object.
 */
interface FolderOwnerInterface 
{
	public function getUsername();
}
