<?php

/*
 * This file is part of the CMediaDriveBundle
 *
 * (c) Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\DocumentType;

use CMedia\Bundle\DriveBundle\DocumentType\Interfaces\DocumentTypeInterface;

/**
 * ImageType
 * 
 * @author Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @package CMedia\Bundle\DriveBundle\DocumentType
 * @license MIT http://opensource.org/licenses/MIT
 * @copyright Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @version v0.2.0
 */
class ImageType implements DocumentTypeInterface
{
    public static $name = 'image';

    public static $mimes = array(
        'image/gif',
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/svg+xml',
        'image/tiff',
        'image/vnd.microsoft.icon',
        'image/vnd.wap.wbmp',
    );

    public static function getTypes()
    {
        return array(self::$name => self::$mimes);
    }
}
