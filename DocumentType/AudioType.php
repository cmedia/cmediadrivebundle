<?php

/*
 * This file is part of the CMediaDriveBundle
 *
 * (c) Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\DocumentType;

use CMedia\Bundle\DriveBundle\DocumentType\Interfaces\DocumentTypeInterface;

/**
 * AudioType
 * 
 * @author Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @package CMedia\Bundle\DriveBundle\DocumentType
 * @license MIT http://opensource.org/licenses/MIT
 * @copyright Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @version v0.2.0
 */
class AudioType implements DocumentTypeInterface
{
    protected static $name = 'audio';

    protected static $mimes = array(
        'audio/basic',
        'audio/L24',
        'audio/mp4',
        'audio/mpeg',
        'audio/ogg',
        'audio/vorbis',
        'audio/x-ms-wma',
        'audio/x-ms-wax',
        'audio/vnd.rn-realaudio',
        'audio/vnd.wave',
        'audio/webm',
    );

    public static function getTypes()
    {
        return array(self::$name => self::$mimes);
    }
}
