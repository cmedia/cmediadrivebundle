<?php

/*
 * This file is part of the CMediaDriveBundle
 *
 * (c) Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\DocumentType;

use CMedia\Bundle\DriveBundle\DocumentType\Intefaces\DocumentTypeRecognizerInterface;
use CMedia\Bundle\DriveBundle\DocumentType\Intefaces\DocumentTypeInterface;
use CMedia\Bundle\DriveBundle\DocumentType\DocumentType;

class DocumentTypeRecognizer implements DocumentTypeRecognizerInterface
{
    protected static $instance;
    protected static $documentTypes;
    protected static $type = null;

    /**
     * Recognize the type of document and return it 
     * @param  \CMedia\Bundle\DriveBundle\Entity\Document $document newly created document
     * @return string the name of type 
     */
    public static function recognize(\CMedia\Bundle\DriveBundle\Entity\Document $document)
    {
        self::setDocumentTypes();

        $documentType = self::searchForType($document->getFile());

        return $documentType === false ? null : $documentType;
    }

    protected static function searchForType()
    {
        foreach (self::$documentTypes as $type => $mimes) {
            if (in_array($file->getMimeType(), $mimes)) {
                self::$type = $type;
            }
        }

        return self::$type;
    }

    protected static function setDocumentTypes()
    {
        self::$documentTypes = DocumentType::getTypes();
    }
}
