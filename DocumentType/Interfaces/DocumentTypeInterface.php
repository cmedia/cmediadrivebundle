<?php

/*
 * This file is part of the CMediaDriveBundle
 *
 * (c) Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\DocumentType\Interfaces;

/**
 * DocumentTypeInterface
 * 
 * @author Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @package CMediaDriveBundle\DocumentType\Interfaces
 * @license MIT http://opensource.org/licenses/MIT
 * @copyright Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @version v0.2.0
 */
interface DocumentTypeInterface
{
    /**
     * Get known types array
     * @return array 
     */
    public static function getTypes();
}
