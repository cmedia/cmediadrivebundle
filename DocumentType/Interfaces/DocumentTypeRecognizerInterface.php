<?php

/*
 * This file is part of the CMediaDriveBundle
 *
 * (c) Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\DocumentType\Interfaces;

interface DocumentTypeRecognizerInterface
{
    /**
     * Recognize the type of document and return it 
     * @param  \CMedia\Bundle\DriveBundle\Entity\Document $document newly created document
     * @return string the name of type 
     */
    public static function recognize(\CMedia\Bundle\DriveBundle\Entity\Document $document);
}
