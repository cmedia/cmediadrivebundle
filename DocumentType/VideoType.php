<?php

/*
 * This file is part of the CMediaDriveBundle
 *
 * (c) Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\DocumentType;

use CMedia\Bundle\DriveBundle\DocumentType\Interfaces\DocumentTypeInterface;

/**
 * VideoType
 * 
 * @author Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @package CMedia\Bundle\DriveBundle\DocumentType
 * @license MIT http://opensource.org/licenses/MIT
 * @copyright Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @version v0.2.0
 */
class VideoType implements DocumentTypeInterface
{
    protected static $name = 'video';

    protected static $mimes = array(
        'video/mpeg',
        'video/mp4',
        'video/ogg',
        'video/quicktime',
        'video/webm',
        'video/x-ms-wmv',
        'video/x-flv',
    );

    public static function getTypes()
    {
        return array(self::$name => self::$mimes);
    }
}
