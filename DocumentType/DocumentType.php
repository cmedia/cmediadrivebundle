<?php

/*
 * This file is part of the CMediaDriveBundle
 *
 * (c) Alexandr Jeliuc <jeliucalexandr@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CMedia\Bundle\DriveBundle\DocumentType;

use CMedia\Bundle\DriveBundle\DocumentType\Interfaces\DocumentTypeInterface;

use CMedia\Bundle\DriveBundle\DocumentType\ImageType;
use CMedia\Bundle\DriveBundle\DocumentType\AudioType;
use CMedia\Bundle\DriveBundle\DocumentType\VideoType;

/**
 * DocumentType
 * 
 * @author Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @package CMedia\Bundle\DriveBundle\DocumentType
 * @license MIT http://opensource.org/licenses/MIT
 * @copyright Alexandr Jeliuc <jeliucalexandr@gmail.com>
 * @version v0.2.0
 */
class DocumentType implements DocumentTypeInterface
{
    public static $name = 'document';

    public static function getTypes()
    {

    }

    public static function setTypes()
    {
        return array_merge(ImageType::getTypes(), AudioType::getTypes(), VideoType::getTypes());
    }
}
